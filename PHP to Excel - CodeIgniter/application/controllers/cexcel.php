<?php
/**
*
*/
class cexcel extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Musers');
		$this->load->library('export_excel');
	}

	public function dExcel(){
		$result = $this->Musers->getUsers();
		$this->export_excel->to_excel($result, 'lista_de_personas');
	}
}
